SHELL := /bin/bash

export OBJC_DISABLE_INITIALIZE_FORK_SAFETY=YES

.PHONY: help
help: ## this help
	@echo "Please use 'make <target>' where <target> is one of"
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-32s\033[0m %s\n", $$1, $$2}'

hcloud-get-images: ## Lists all Images (backups, defaults, custom images)
	@curl -s -H "Authorization: Bearer $(HCLOUD_TOKEN)" \
		'https://api.hetzner.cloud/v1/images' | jq

hcloud-get-ssh-keys: ## Lists SSH Keys
	@curl -s -H "Authorization: Bearer $(HCLOUD_TOKEN)" \
		'https://api.hetzner.cloud/v1/ssh_keys' | jq

hcloud-get-servers: ## Lists Servers in Project
	@curl -s -H "Authorization: Bearer $(HCLOUD_TOKEN)" \
		'https://api.hetzner.cloud/v1/servers' | jq

hcloud-get-floating-ips: ## Lists all Floating IPs
	@curl -s -H "Authorization: Bearer $(HCLOUD_TOKEN)" \
		'https://api.hetzner.cloud/v1/floating_ips' | jq

hcloud-get-all-actions: ## get all Actions
	@curl -s -H "Authorization: Bearer $(HCLOUD_TOKEN)" \
		'https://api.hetzner.cloud/v1/actions' | jq

update-git-submodules: ## updates all the Git submodules
	@git submodule update --init --recursive

hugo-local-server: ## starts the local hugo development server
	@hugo server --buildDrafts
