+++
menutitle = "1.1 Vorbereitungen"
title = "Vorbereitungen"
date = 2020-10-31T15:05:43+01:00
weight = 2
chapter = false
+++

#### Schritt 1: Terraform installieren

{{< notice note >}}
Wenn Du kein MacOS verwendest, [schau bitte hier nach](https://learn.hashicorp.com/tutorials/terraform/install-cli), um zu erfahren, wie Du Terraform installieren kannst.
{{< /notice >}}

```shell
$ brew tap hashicorp/tap
$ brew install hashicorp/tap/terraform
```

#### Schritt 2: Hetzner Cloud einrichten

Sofern Du noch kein Hetzner Kunde bist, richte Dir bitte zunächst einen [Account](https://accounts.hetzner.com/signUp) ein.

#### Schritt 3: API Key erstellen