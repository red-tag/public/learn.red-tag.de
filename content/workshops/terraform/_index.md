+++
title = "Terraform"
date = 2020-11-01T11:53:56+01:00
weight = 0
chapter = true
pre = '<i class="fab fa-mixcloud"></i> '
+++

### Infrastructure as Code

# Terraform

Wer hätte das gedacht: nicht nur für die "großen Clouds" wie AWS, Azure oder Google kann man Terraform einsetzen. 
Auf den folgenden Seiten erfährst Du, wie Du ein Test Setup in der [Hetzner Cloud](https://www.hetzner.com/de/cloud) aufbaust, erweiterst und wieder abbaust.