+++
title = "Workshops"
date = 2020-11-01T11:53:56+01:00
weight = 0
chapter = false
pre = '<i class="fas fa-angle-double-right"></i> '
+++

### Schritt für Schritt zum Ergebnis

Ich möchte Euch aus meiner täglichen Praxis berichten und Euch Anregungen und Tipps geben. 

{{< notice info >}}
Du solltest Erfahrungen mit Mac OSX und ein solides Verständnis von DevOps mitbringen.
{{< /notice >}}