+++
menutitle = "99. Cheatsheet"
title = "Cheatsheet"
weight = 99
chapter = false
+++

##### API Ressourcen auflisten

{{< highlight bash >}}
kubectl api-resources \
  -o wide --sort-by=name
{{< / highlight >}}

##### Check Nodes Conditions

{{< highlight bash >}}
kubectl get nodes \
  -o jsonpath='{range .items[*]}{@.metadata.name}{"\n"}{range @.status.conditions[*]}{@.type}={@.status}{"\n"}{end}{end}'
{{< / highlight >}}

##### YAML Manifest generieren (Beispiel: Deloyment)

{{< notice note >}}
Der entscheidende Kniff hierbei ist die `--dry-run=client` Option!
{{< /notice >}}

{{< highlight bash >}}
kubectl create deployment \
  --image=nginx nginx \
  --dry-run=client \
  -o yaml
{{< / highlight >}}

